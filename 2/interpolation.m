function Y = finite_difference (X)
    if (!isvector(X))
        error ("X is not vector");
    endif
    n = length (X);
    Y = zeros (n);
    Y(:,1) = X;
    m = n;
    for i = 2:n
        Y(1:m-1,i) = Y(2:m,i-1) - Y(1:m-1,i-1);
        --m;
    endfor
endfunction

function y = common_interpolation (Yp, U)
    n = length (Yp);
    F = cumprod ([1, 1:n - 1]);
    y = sum (U .* Yp ./ F);
endfunction

function y = newton_interpolation1 (Yp, u)
    n = length (Yp);
    U = cumprod ([1, u - (0:n-2)]);
    y = common_interpolation(Yp, U);
endfunction

function y = newton_interpolation2 (Yp, u)
    n = length (Yp);
    U = cumprod ([1, u + (0:n-2)]);
    y = common_interpolation(Yp, U);
endfunction

function y = gauss_interpolation1 (YS, p, u)
    n = size (YS, 2);
    T = [1, u];
    for i = 1:n
        T = [T, u - i, u + i];
    endfor
    Yp = [];
    i = p;
    for j = 1:n
        if (i <= 0)
            break;
        endif
        Yp = [Yp, YS(i, j)];
        if (mod (j, 2) == 0)
            --i;
        endif
    endfor
    n = length (Yp);
    y = common_interpolation(Yp, cumprod (T(1:n)));
endfunction

function y = gauss_interpolation2 (YS, p, u)
    n = size (YS, 2);
    T = [1, u];
    for i = 1:n
        T = [T, u + i, u - i];
    endfor
    Yp = [YS(p, 1)];
    i = p - 1;
    for j = 2:n
        if (i <= 0)
            break;
        endif
        Yp = [Yp, YS(i, j)];
        if (mod (j, 2) == 1)
            --i;
        endif
    endfor
    n = length (Yp);
    y = common_interpolation(Yp, cumprod (T(1:n)));
endfunction

function YI = interpolation (X, Y, XI)
    n = length (X);
    if (n < 2)
        error ("Length of X is too small");
    endif
    if (length (Y) != n)
        error ("Length of X and Y are not equal");
    endif

    dX = X(2) - X(1);
    YS = finite_difference (Y);

    m = length (XI);
    YI = zeros (size (XI));
    for i = 1:m
        x = XI(i);
        p = 1;
        for j = 2:n
            if (abs (x - X(p)) > abs (x - X(j)))
                p = j;
            endif
        endfor
        u = (x - X(p)) / dX;
        if (x <= X(1))
            u = (x - X(1)) / dX;
            YI(i) = newton_interpolation1 (YS(1,:), u);
        elseif (x >= X(n))
            u = (x - X(n)) / dX;
            YI(i) = newton_interpolation2 (fliplr (diag (fliplr (YS))'), u);
        else
            if (x > X(p))
                YI(i) = gauss_interpolation1 (YS, p, u);
            else
                YI(i) = gauss_interpolation2 (YS, p, u);
            endif
        endif
    endfor
endfunction
